



# 写在前面
>我是bug菌，[CSDN](https://blog.csdn.net/weixin_43970743) | [掘金](https://juejin.cn/user/695333581765240) | [InfoQ](https://www.infoq.cn/profile/4F581734D60B28/publish) | [51CTO](https://blog.51cto.com/u_15700751) | [华为云](https://bbs.huaweicloud.com/community/usersnew/id_1582617489455371) | [阿里云](https://developer.aliyun.com/profile/uolxikq5k3gke) | [腾讯云](https://cloud.tencent.com/developer/user/10216480/articles) 等社区博客专家，C站博客之星Top30，华为云2023年度十佳博主，掘金多年度人气作者Top40，51CTO年度博主Top12，掘金/InfoQ/51CTO等社区优质创作者；全网粉丝合计 **20w+**；硬核微信公众号[「猿圈奇妙屋」](https://bbs.csdn.net/topics/612438251)，欢迎你的加入！免费白嫖最新BAT互联网公司面试真题、4000G PDF电子书籍、简历模板等海量资料，你想要的我都有，关键是你不来拿。

&emsp;&emsp;为了便于同学们快速定位文章并系统性学习，bug菌几乎是几宿未睡才整理出了该专栏[「滚雪球学Java」](https://luoyong.blog.csdn.net/category_12294148_2.html) ，目前已更新上了已发布文章的有效地址，快去吸收学习吧。论专业，这才叫！！


更多技术干货，可以扫描下方二维码，关注公众号：[「猿圈奇妙屋」](https://bbs.csdn.net/topics/612438251)，每天定时推送优质技术文章！

![猿圈奇妙屋](image/%E5%85%AC%E4%BC%97%E5%8F%B7%E6%8E%A8%E5%B9%BF.jpg)


# 🔥🔥 JavaSE 系列教程，2024年国内最系统+最强 🔥🔥 

> ⚡作者：bug菌，全栈开发斗宗老兵，✏️[博客地址](https://blog.csdn.net/weixin_43970743?type=blog)，希望你能有所收获 ⚡

# 🌜「滚雪球学Java」 零基础入门Java教学，已完成文章清单(80+)
&emsp;&emsp;专栏包含了14个大章节，然后每个章节又分数篇以带你循序渐进的学习；以下是专栏中分章节的系列文章汇集，以最快的速度带领小白吃透Java，全力助你打造速成职场初学者。

---
### 🐌一、前序（3节）
&emsp;&emsp;这三个小结目录是Java零基础入门中非常重要的部分。第一个目录让我们认识和了解Java编程语言；第二个目录则是告诉我们如何在我们的计算机上配置Java开发环境；而第三个目录则是教我们如何使用Java Development Kit（JDK）和Java Runtime Environment（JRE）来开发和执行Java应用程序，以及如何理解Java虚拟机（JVM）的工作原理和作用。
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|滚雪球学Java(01) | [认识Java](https://blog.csdn.net/weixin_43970743/article/details/132619947)| ✔️ |❌️  |
|滚雪球学Java(02) | [Java环境配置](https://blog.csdn.net/weixin_43970743/article/details/132635434)| ✔️ |❌️  |
|滚雪球学Java(03) | [区别JDK、JRE和JVM](https://blog.csdn.net/weixin_43970743/article/details/132645708)| ✔️ |❌️  | 
💥「滚雪球学Java」 | [**Java前序篇(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134059587)| ✔️ |✔️  |

&emsp;&emsp;总体而言，这3节为Java新手提供了一个完整的学习和理解Java编程语言的基础。学会了它们，我们可以继续深入学习Java的更高级部分，例如：数据类型、变量、运算符、条件语句、循环语句、函数、数组、面向对象编程和异常处理等等。

---
### 🐌二、基础程序设计（24节）
Java SE中针对基础程序设计课程可以细分为以下24小节进行讲解：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|滚雪球学Java(04) | [JDK、IntelliJ IDEA的安装和环境变量配置](https://blog.csdn.net/weixin_43970743/article/details/133104124)| ✔️ | ❌️  |
|滚雪球学Java(05) | [Java关键字、标识符和命名规范](https://blog.csdn.net/weixin_43970743/article/details/133104401)| ✔️ |❌️  |
|滚雪球学Java(06) | [基本数据类型和取值范围](https://blog.csdn.net/weixin_43970743/article/details/133104554)| ✔️ |❌️  |
|滚雪球学Java(07) | [基本类型转换、包装类、自动装箱、自动拆箱](https://blog.csdn.net/weixin_43970743/article/details/133104643)| ✔️ |❌️  |
|滚雪球学Java(08) | [变量、常量及其作用域](https://blog.csdn.net/weixin_43970743/article/details/133104680)| ✔️ |❌️  |
|滚雪球学Java(09) | [运算符、表达式和语句](https://blog.csdn.net/weixin_43970743/article/details/133104750)| ✔️ |❌️  |
|滚雪球学Java(09-1) | [算术运算符](https://blog.csdn.net/weixin_43970743/article/details/134300081)| ✔️ |❌️  |
|滚雪球学Java(09-2) | [关系运算符](https://blog.csdn.net/weixin_43970743/article/details/134300283)| ✔️ |❌️  |
|滚雪球学Java(09-3) | [逻辑运算符](https://blog.csdn.net/weixin_43970743/article/details/134300359)| ✔️ |❌️  |
|滚雪球学Java(09-4) | [位运算符](https://blog.csdn.net/weixin_43970743/article/details/134300446)| ✔️ |❌️  |
|滚雪球学Java(09-5) | [赋值运算符](https://blog.csdn.net/weixin_43970743/article/details/134300562)| ✔️ |❌️  |
|滚雪球学Java(09-6) | [条件运算符](https://blog.csdn.net/weixin_43970743/article/details/134300657)| ✔️ |❌️  |
|滚雪球学Java(09-7) | [instanceof 运算符](https://blog.csdn.net/weixin_43970743/article/details/134300747)| ✔️ |❌️  |
|滚雪球学Java(09-8) | [单目运算符](https://blog.csdn.net/weixin_43970743/article/details/134300805)| ✔️ |❌️  |
|滚雪球学Java(09-9) | [三目运算符](https://blog.csdn.net/weixin_43970743/article/details/134300851)| ✔️ |❌️  |
|滚雪球学Java(09-10) | [Lambda运算符](https://blog.csdn.net/weixin_43970743/article/details/134300919)| ✔️ |❌️  |
|滚雪球学Java(10) | [注释](https://blog.csdn.net/weixin_43970743/article/details/133104811)| ✔️ |❌️  |
|滚雪球学Java(11) | [顺序结构](https://blog.csdn.net/weixin_43970743/article/details/133104910)| ✔️ |❌️  |
|滚雪球学Java(12) | [if条件语句](https://blog.csdn.net/weixin_43970743/article/details/133105035)| ✔️ |❌️  |
|滚雪球学Java(13) | [switch条件语句](https://blog.csdn.net/weixin_43970743/article/details/133105158)| ✔️ |❌️  |
|滚雪球学Java(14) | [for循环语句](https://blog.csdn.net/weixin_43970743/article/details/133105240)| ✔️ |❌️  |
|滚雪球学Java(15) | [while循环语句](https://blog.csdn.net/weixin_43970743/article/details/133105306)| ✔️ |❌️  |
|滚雪球学Java(16) | [do-while循环语句](https://blog.csdn.net/weixin_43970743/article/details/133105347)| ✔️ |❌️  |
|滚雪球学Java(17) | [Java 的循环退出语句 break、continue](https://blog.csdn.net/weixin_43970743/article/details/133105531)| ✔️ |❌️  |
|滚雪球学Java(18) | [Java 堆栈](https://blog.csdn.net/weixin_43970743/article/details/133105690)| ✔️ |❌️  |
|滚雪球学Java(19) | [Java 内存机制](https://blog.csdn.net/weixin_43970743/article/details/133105774)| ✔️ |❌️  |
|滚雪球学Java(20) | [泛型和枚举](https://blog.csdn.net/weixin_43970743/article/details/133105846)| ✔️ |❌️  |
|滚雪球学Java(21) | [正则表达式](https://blog.csdn.net/weixin_43970743/article/details/133105968)| ✔️ |❌️  |
|滚雪球学Java(22) | [序列化和反序列化](https://blog.csdn.net/weixin_43970743/article/details/133106119)| ✔️ |❌️  |
|滚雪球学Java(23) | [包机制](https://blog.csdn.net/weixin_43970743/article/details/133106202)| ✔️ |❌️  |
|滚雪球学Java(24) | [反射](https://blog.csdn.net/weixin_43970743/article/details/133106250)| ✔️ |❌️  |
|滚雪球学Java(25) | [动态代理](https://blog.csdn.net/weixin_43970743/article/details/133106310)| ✔️ |❌️  |
|滚雪球学Java(26) | [进制转换](https://blog.csdn.net/weixin_43970743/article/details/133106350)| ✔️ |❌️  |
|💥「滚雪球学Java」 | [**基础程序设计(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134050837)| ✔️ |✔️  |

| 备注：如上【是否完成】所对应列中✔️表示我所对应行的文章已撰写完成；【是否打卡学习】所对应列中❌️代表你所在行对应的文章未完成学习。若已经学习记得在评论区留下你学习完成的序号，即可，我会及时统计收集修改完成状态哒。|
---
### 🐌三、数组（8节）
Java SE课程中，数组可以分为以下几个章节进行内容学习：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|滚雪球学Java(27)| [从零开始学习数组：定义和初始化](https://blog.csdn.net/weixin_43970743/article/details/133126804)| ✔️ |❌️  |
|滚雪球学Java(28)| [轻松掌握数组：访问和遍历技巧](https://blog.csdn.net/weixin_43970743/article/details/133129356)| ✔️ |❌️  |
|滚雪球学Java(29)| [数组长度和排序算法：让你的程序更高效](https://blog.csdn.net/weixin_43970743/article/details/133129682)| ✔️ |❌️  |
|滚雪球学Java(30)| [多维数组：定义和初始化一次搞定](https://blog.csdn.net/weixin_43970743/article/details/133131632)| ✔️ |❌️  |
|滚雪球学Java(31)| [玩转多维数组：高效访问和遍历](https://blog.csdn.net/weixin_43970743/article/details/133131945)| ✔️ |❌️  |
|滚雪球学Java(32)| [如何理解和实现稀疏数组](https://blog.csdn.net/weixin_43970743/article/details/133135530)| ✔️ |❌️  |
|  滚雪球学Java(33)| [数组算法大揭秘：应用案例实战分享](https://blog.csdn.net/weixin_43970743/article/details/133135710)| ✔️ |❌️  |
|💥「滚雪球学Java」  |  [  **数组(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051411)  |  ✔️  |  ✔️   |

&emsp;&emsp;其中每个章节都包含了不同的知识点和代码实例，需要认真学习和掌握。
### 🐌四、方法函数（8节）
Java SE中方法函数可以分为以下几个大章节进行内容学习：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(34)  |  [  探究Java方法的神奇魔法和参数传递奥秘](https://blog.csdn.net/weixin_43970743/article/details/133136089) |  ✔️  |  ❌️   |
| [滚雪球学Java(35)  |  [  揭秘Java方法的返回值，从void到诸多数据类型](https://blog.csdn.net/weixin_43970743/article/details/133138296)  |  ✔️  |  ❌️   |
|  滚雪球学Java(36)  |  [  玩转Java方法重载和可变参数，让你的代码更灵活](https://blog.csdn.net/weixin_43970743/article/details/133138729)   |  ✔️  |  ❌️   |
|  滚雪球学Java(37)  |  [  深入了解Java方法作用域和生命周期，让你写出更高效的代码](https://blog.csdn.net/weixin_43970743/article/details/133138937)   |  ✔️  |  ❌️   |
|  滚雪球学Java(38)  |  [  探索Java递归的无穷魅力，解决复杂问题轻松搞定](https://blog.csdn.net/weixin_43970743/article/details/133139268)  |  ✔️  |  ❌️   |
|  滚雪球学Java(39)  |  [  学会Java异常处理，让你的程序健壮无比](https://blog.csdn.net/weixin_43970743/article/details/133139549)  |  ✔️  |  ❌️   |
|  滚雪球学Java(40)  |  [  解读Java面向对象编程中的方法和继承，打造可维护的代码库](https://blog.csdn.net/weixin_43970743/article/details/133139775)  |  ✔️  |  ❌️   |
|  滚雪球学Java(41)  |  [  Lambda表达式和方法引用：提高代码可读性和简洁性的神器](https://blog.csdn.net/weixin_43970743/article/details/133140220)  |  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [  **方法函数(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051429)  |  ✔️  |  ✔️   |

### 🐌五、常用类（15节）
Java SE课程中，常用类可以分为以下几个大章节进行内容学习：
#### 💫1. Java语言基础类：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(42)  |  [  探索对象的奥秘：解析Java中的Object类](https://blog.csdn.net/weixin_43970743/article/details/133685738)    |  ✔️  |  ❌️   |
|  滚雪球学Java(43)  |  [  探究 Java 中的 Class 类：透视类的本质和实现原理](https://blog.csdn.net/weixin_43970743/article/details/133696929)  |  ✔️  |  ❌️   |
|  滚雪球学Java(44)  |  [  掌握Java编程的关键：深入解析System类](https://blog.csdn.net/weixin_43970743/article/details/133740923)  |  ✔️  |  ❌️   |
|  滚雪球学Java(45)  |  [  探秘Java Runtime类：深入了解JVM运行时环境](https://blog.csdn.net/weixin_43970743/article/details/133745604)  |  ✔️  |  ❌️   |
|  滚雪球学Java(46)  |  [  揭开数学的神秘面纱：探索Java中Math类的奇妙世界](https://blog.csdn.net/weixin_43970743/article/details/133746868)  |  ✔️  |  ❌️   |
#### 💫2. 字符串操作：  
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(47) |  [  String类教程：如何在Java中使用字符串操作](https://blog.csdn.net/weixin_43970743/article/details/133747015) |  ✔️  |  ❌️   |
|  滚雪球学Java(48) |  [  面向对象编程中的StringBuffer类详解](https://blog.csdn.net/weixin_43970743/article/details/133748184) |  ✔️  |  ❌️   |
|  滚雪球学Java(49) |  [  如何使用StringBuilder类在Java中高效地处理字符串？](https://blog.csdn.net/weixin_43970743/article/details/133784859) |  ✔️  |  ❌️   |
|  滚雪球学Java(50)  |  [  理解Java中String、StringBuilder和StringBuffer的区别与选择](https://blog.csdn.net/weixin_43970743/article/details/133786264) |  ✔️  |  ❌️   |
#### 💫3. 日期和时间处理：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(51)  |  [  掌握Java Date类：用法、弊端与时间戳转换技巧，助你轻松处理日期时间](https://blog.csdn.net/weixin_43970743/article/details/133862471) |  ✔️  |  ❌️   |
|  滚雪球学Java(52)  |  [  一步一步教你使用Java Calendar类进行日期计算](https://blog.csdn.net/weixin_43970743/article/details/133877920) |  ✔️  |  ❌️   |
|  滚雪球学Java(53)  |  [  从入门到精通：SimpleDateFormat类高深用法，让你的代码更简洁！](https://blog.csdn.net/weixin_43970743/article/details/133891438) |  ✔️  |  ❌️   |
#### 💫4. 数学操作：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(54)  |  [  从零开始学习Java中的Math类，轻松解决数学难题](https://blog.csdn.net/weixin_43970743/article/details/133909894) |  ✔️  |  ❌️   |
|  滚雪球学Java(55)  |  [  想让你的程序更有趣？加上这个Java的Random类的小技巧！](https://blog.csdn.net/weixin_43970743/article/details/133911457) |  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [  **常用类(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051452) |  ✔️  |  ✔️   |

&emsp;&emsp;这些小节涵盖了JavaSE课程中基础类库中的常用类和操作。通过学习这些内容，可以深入理解Java语言的基本类和常用操作。

---
### 🐌六、集合（24节）
Java SE课程中，集合可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(56)  |  [ Collection接口](https://blog.csdn.net/weixin_43970743/article/details/133997839) |  ✔️  |  ❌️   |
|  滚雪球学Java(57)  |  [ List接口](https://blog.csdn.net/weixin_43970743/article/details/134224453) |  ✔️  |  ❌️   |
|  滚雪球学Java(58)  |  [ ArrayList](https://blog.csdn.net/weixin_43970743/article/details/134224553) |  ✔️  |  ❌️   |
|  滚雪球学Java(59)  |  [ LinkedList](https://blog.csdn.net/weixin_43970743/article/details/134224621) |  ✔️  |  ❌️   |
|  滚雪球学Java(60)  |  [ Vector](https://blog.csdn.net/weixin_43970743/article/details/134224684) |  ✔️  |  ❌️   |
|  滚雪球学Java(61)  |  [ Set接口](https://blog.csdn.net/weixin_43970743/article/details/134224793) |  ✔️  |  ❌️   |
|  滚雪球学Java(62)  |  [ HashSet](https://blog.csdn.net/weixin_43970743/article/details/134224862) |  ✔️  |  ❌️   |
|  滚雪球学Java(63)  |  [ TreeSet](https://blog.csdn.net/weixin_43970743/article/details/134224936) |  ✔️  |  ❌️   |
|  滚雪球学Java(64)  |  [ LinkedHashSet](https://blog.csdn.net/weixin_43970743/article/details/134224978) |  ✔️  |  ❌️   |
|  滚雪球学Java(65)  |  [ Map接口](https://blog.csdn.net/weixin_43970743/article/details/134231576) |  ✔️  |  ❌️   |
|  滚雪球学Java(66)  |  [ HashMap](https://blog.csdn.net/weixin_43970743/article/details/134231674) |  ✔️  |  ❌️   |
|  滚雪球学Java(67)  |  [ treeMap](https://blog.csdn.net/weixin_43970743/article/details/134243482) |  ✔️  |  ❌️   |
|  滚雪球学Java(68)  |  [ LinkedHashMap](https://blog.csdn.net/weixin_43970743/article/details/134243631) |  ✔️  |  ❌️   |
|  滚雪球学Java(65-1)  |  [ Hashtable](https://blog.csdn.net/weixin_43970743/article/details/134243855) |  ✔️  |  ❌️   |
|  滚雪球学Java(65-2)  |  [ WeakHashMap](https://blog.csdn.net/weixin_43970743/article/details/134243862) |  ✔️  |  ❌️   |
|  滚雪球学Java(65-3)   |  [ IdentityHashMap](https://blog.csdn.net/weixin_43970743/article/details/134243890) |  ✔️  |  ❌️   |
|  滚雪球学Java(65-4)  |  [ ConcurrentHashMap](https://blog.csdn.net/weixin_43970743/article/details/134243901) |  ✔️  |  ❌️   |
|  [滚雪球学Java(65-5)  |  [ Properties](https://blog.csdn.net/weixin_43970743/article/details/134243905) |  ✔️  |  ❌️   |
|  滚雪球学Java(69)  |  [ ConcurrentLinkedQueue](https://blog.csdn.net/weixin_43970743/article/details/134231498) |  ✔️  |  ❌️   |
|  滚雪球学Java(70)  |  [ PriorityQueue](https://blog.csdn.net/weixin_43970743/article/details/134231359) |  ✔️  |  ❌️   |
|  滚雪球学Java(71)  |  [ ArrayBlockingQueue](https://blog.csdn.net/weixin_43970743/article/details/134225056) |  ✔️  |  ❌️   |
|  滚雪球学Java(72)  |  [ Queue](https://blog.csdn.net/weixin_43970743/article/details/134225019) |  ✔️  |  ❌️   |
|  滚雪球学Java(73)  |  [ 弱引用和软引用](https://blog.csdn.net/weixin_43970743/article/details/134243925) |  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [ **集合(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051508)|  ✔️  |  ✔️   |

---
### 🐌七、输入输出IO（7节）

Java SE课程中，Java IO可以分为以下几个大章节进行内容学习：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(74)  |  [  深入理解JavaSE输入输出流](https://blog.csdn.net/weixin_43970743/article/details/135250104) |  ✔️  |  ❌️   |
|  滚雪球学Java(75)  |  [  轻松学会文件读写技巧](https://blog.csdn.net/weixin_43970743/article/details/135250710)|  ✔️  |  ❌️   |
|  滚雪球学Java(76)  |  [  对象序列化和反序列化](https://blog.csdn.net/weixin_43970743/article/details/135250976)|  ✔️  |  ❌️   |
|  滚雪球学Java(77)  |  [  字符编码](https://blog.csdn.net/weixin_43970743/article/details/135251396)|  ✔️  |  ❌️   |
|  滚雪球学Java(78) |  [  标准输入输出](https://blog.csdn.net/weixin_43970743/article/details/135251626)|  ✔️  |  ❌️   |
|  滚雪球学Java(79) |  [  文件压缩和解压缩](https://blog.csdn.net/weixin_43970743/article/details/135251881)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」 |  [  **输入输出IO(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051611)|  ✔️  |  ✔️   |
---
### 🐌八、多线程（6节）

Java SE课程中，多线程可以分为以下几个大章节进行内容学习：
|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(80)  |  [  线程的创建、启动和终止](https://blog.csdn.net/weixin_43970743/article/details/135252784)|  ✔️   |❌️   |
|  滚雪球学Java(81)  |  [  线程同步和互斥](https://blog.csdn.net/weixin_43970743/article/details/142058983)|  ✔️  |  ❌️   |
|  滚雪球学Java(82)  |  [  线程通信](https://blog.csdn.net/weixin_43970743/article/details/137244134)|  ✔️  |  ❌️   |
|  滚雪球学Java(83)  |  [  线程池](https://blog.csdn.net/weixin_43970743/article/details/137244149)|  ✔️  |  ❌️   |
|  滚雪球学Java(84)  |  [  并发集合](https://blog.csdn.net/weixin_43970743/article/details/137244169)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [  **多线程(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051661)|  ✔️  |  ✔️   |

---
### 🐌九、JDBC（Java Database Connectivity）（5节）

Java SE课程中，JDBC可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(85)  |  [  数据库连接](https://blog.csdn.net/weixin_43970743/article/details/137248548)|  ✔️  |  ❌️   |
|  滚雪球学Java(86)  |  [  SQL的执行和结果处理](https://blog.csdn.net/weixin_43970743/article/details/137248633)|  ✔️  |  ❌️   |
|  滚雪球学Java(87)  |  [  事务处理](https://blog.csdn.net/weixin_43970743/article/details/142059394)|  ✔️  |  ❌️   |
|  滚雪球学Java(88)  |  [  数据库连接池](https://blog.csdn.net/weixin_43970743/article/details/142059557)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [  **JDBC(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051682)|  ✔️  |  ✔️   |

### 🐌十、GUI编程（7节）

Java SE课程中，GUI编程可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(89)  |  [  AWT（Abstract Window Toolkit）](https://blog.csdn.net/weixin_43970743/article/details/142059738)|  ✔️  |  ❌️   |
|  滚雪球学Java(90)  |  [  Swing](https://blog.csdn.net/weixin_43970743/article/details/142059885)|  ✔️  |  ❌️   |
|  滚雪球学Java(91)  |  [  组件](https://blog.csdn.net/weixin_43970743/article/details/142060381)|  ✔️  |  ❌️   |
|  滚雪球学Java(92)  |  [  布局管理器](https://blog.csdn.net/weixin_43970743/article/details/137248884)|  ✔️  |  ❌️   |
|  滚雪球学Java(93)  |  [  事件处理](https://blog.csdn.net/weixin_43970743/article/details/137249031)|  ✔️  |  ❌️   |
|  滚雪球学Java(94)  |  [  JavaFX](https://blog.csdn.net/weixin_43970743/article/details/137249048)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [  **GUI编程(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051713)|  ✔️  |  ✔️   |

---
### 🐌十一、网络编程（6节）

Java SE课程中，网络编程可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(95)  |  [ TCP和UDP协议](https://blog.csdn.net/weixin_43970743/article/details/137249079)|  ✔️  |  ❌️   |
|  滚雪球学Java(96)  |  [ Socket编程](https://blog.csdn.net/weixin_43970743/article/details/137249181)|  ✔️  |  ❌️   |
|  滚雪球学Java(97)  |  [ URL和URLConnection](https://blog.csdn.net/weixin_43970743/article/details/137249208)|  ✔️  |  ❌️   |
|  滚雪球学Java(98)  |  [ HTTP和HTTPS](https://blog.csdn.net/weixin_43970743/article/details/137249221)|  ✔️  |  ❌️   |
|  滚雪球学Java(99)  |  [ Web服务（SOAP、RESTful）](https://blog.csdn.net/weixin_43970743/article/details/137249230)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [ **网络编程(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051732)|  ✔️  |  ✔️   |

---
### 🐌十二、内存管理和垃圾回收（4节）

Java SE课程中，内存管理和垃圾回收可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(100)  |  [ Java内存模型 ](https://blog.csdn.net/weixin_43970743/article/details/137249263)|  ✔️  |  ❌️   |
|  滚雪球学Java(101)  |  [ 垃圾回收器](https://blog.csdn.net/weixin_43970743/article/details/137249278)|  ✔️  |  ❌️   |
|  滚雪球学Java(102)  |  [ 内存泄漏](https://blog.csdn.net/weixin_43970743/article/details/137249291)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」  |  [ **内存管理和垃圾回收(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051753)|  ✔️  |  ✔️   |

---
### 🐌十三、安全（4节）

Java SE课程中，安全可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(103) |  [ 加密和解密 ](https://blog.csdn.net/weixin_43970743/article/details/137249338)|  ✔️  |  ❌️   |
|  滚雪球学Java(104) |  [ 数字签名和认证 ](https://blog.csdn.net/weixin_43970743/article/details/137249361)|  ✔️  |  ❌️   |
|  滚雪球学Java(105) |  [ 安全管理器 ](https://blog.csdn.net/weixin_43970743/article/details/137249387)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java」 |  [ **安全(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051771)|  ✔️  |  ✔️   |

---
### 🐌十四、国际化（4节）

Java SE课程中，国际化可以分为以下几个大章节进行内容学习：

|序号| 文章 | 是否完成  | 是否打卡学习 |
|--| --|--|--|
|  滚雪球学Java(106) |  [ 本地化和国际化 ](https://blog.csdn.net/weixin_43970743/article/details/137249442)|  ✔️  |  ❌️   |
|  滚雪球学Java(107) |  [ 资源包 ](https://blog.csdn.net/weixin_43970743/article/details/137249459)|  ✔️  |  ❌️   |
|  滚雪球学Java(108) |  [ 日期和时间格式化 ](https://blog.csdn.net/weixin_43970743/article/details/137249486)|  ✔️  |  ❌️   |
|  💥「滚雪球学Java |  [ **国际化(章节汇总)**](https://blog.csdn.net/weixin_43970743/article/details/134051788)|  ✔️  |  ✔️   |

这些小章节涵盖了Java SE体系中的各个重要主题。

---
## 🌀小结

&emsp;&emsp;如上Java SE课程共包含了14个大章节，每个大章节又有若干个小章节，涵盖了Java编程语言的基础、常用类库、集合、输入输出IO、多线程、JDBC、GUI编程、网络编程、内存管理和垃圾回收、安全和国际化等多个方面。这些章节详细讲解了Java语言的各种基本语法、概念和使用方法，以及常用库的功能和用法，对于初学者来说非常有帮助。通过学习这些章节，我们可以深入了解Java编程语言的特点和优点，掌握Java编程的基本技能，为以后深入学习Java和开发Java应用程序打下坚实的基础。无论是对于初学者还是对于有一定基础的开发者，都有着重要的参考价值。

## 💦附录源码
&emsp;&emsp;如上涉及所有源码均已上传同步在[「Gitee」](https://gitee.com/bugjun01/snowball-learning-java)，提供给同学们一对一参考学习，辅助你更迅速的掌握。

## ☀️建议/推荐你

---
&emsp;&emsp;无论你是计算机专业的学生，还是对编程有兴趣的小伙伴，都建议直接毫无顾忌的学习此专栏[「滚雪球学Java」](https://blog.csdn.net/weixin_43970743/category_12294148.html)，bug菌郑重承诺，凡是学习此专栏的同学，均能获取到所需的知识和技能，全网最快速入门Java编程，就像滚雪球一样，越滚越大，指数级提升。并且你还可以加入对应技术交流群，bug菌会亲自进行一切知识点答疑。

## 📣关于我

我是bug菌，[CSDN](https://blog.csdn.net/weixin_43970743) | [掘金](https://juejin.cn/user/695333581765240) | [InfoQ](https://www.infoq.cn/profile/4F581734D60B28/publish) | [51CTO](https://blog.51cto.com/u_15700751) | [华为云](https://bbs.huaweicloud.com/community/usersnew/id_1582617489455371) | [阿里云](https://developer.aliyun.com/profile/uolxikq5k3gke) | [腾讯云](https://cloud.tencent.com/developer/user/10216480/articles) 等社区博客专家，C站博客之星Top30，华为云2023年度十佳博主，掘金多年度人气作者Top40，51CTO年度博主Top12，掘金/InfoQ/51CTO等社区优质创作者；全网粉丝合计 **20w+**；硬核微信公众号[「猿圈奇妙屋」](https://bbs.csdn.net/topics/612438251)，欢迎你的加入！免费白嫖最新BAT互联网公司面试真题、4000G PDF电子书籍、简历模板等海量资料，你想要的我都有，关键是你不来拿。

![猿圈奇妙屋](image/%E5%85%AC%E4%BC%97%E5%8F%B7%E6%8E%A8%E5%B9%BF.jpg)
