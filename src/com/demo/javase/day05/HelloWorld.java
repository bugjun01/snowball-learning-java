package com.demo.javase.day05;

/**
 * @Author bug菌
 * @Date 2023-10-09 22:15
 */
public class HelloWorld {

    public static final int MAX_NUM = 100;
    private int num;

    public HelloWorld(int num) {
        this.num = num;
    }

    public void sayHello() {
        System.out.println("Hello, World!");
    }

    public int add(int x, int y) {
        return x + y;
    }

    public static void main(String[] args) {
        HelloWorld helloWorld = new HelloWorld(10);
        helloWorld.sayHello();
        int sum = helloWorld.add(1, 2);
        System.out.println("Sum is " + sum);
    }

}
