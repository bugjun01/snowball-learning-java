package com.demo.javase.day06;

/**
 * 演示Java中的数据类型和取值范围解析
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:08
 */
public class TestDataStyle {

    public static void main(String[] args) {
        byte myByte = (byte) 129; // 编译错误：超出取值范围
        short myShort = (short) 32768; // 编译错误：超出取值范围
//        int myInt = 2147483648; // 编译错误：超出取值范围
//        long myLong = 9223372036854775808L; // 编译错误：超出取值范围
//
//        float myFloat = 3.40282347E+39F; // 编译错误：超出取值范围
//        double myDouble = 1.7976931348623159E+309; // 编译错误：超出取值范围

        char myChar = '\uffff'; // 取值范围正确

        boolean myBoolean = true; // 取值范围正确

    }


}
