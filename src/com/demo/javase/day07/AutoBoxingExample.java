package com.demo.javase.day07;

/**
 * 自动装箱和自动拆箱
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:41
 */
public class AutoBoxingExample {

    public static void main(String[] args) {
        // 自动装箱
        Integer i = 100;
        System.out.println(i);

        // 自动拆箱
        Integer j = new Integer(100);
        int k = j;
        System.out.println(k);
    }
}
