package com.demo.javase.day07;

/**
 * 基本类型转换
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:37
 */
public class TypeConversionExample {

    public static void main(String[] args) {
        // 自动类型转换
        byte b = 10;
        int i = b;
        System.out.println(i);

        // 强制类型转换
        int j = 100;
        byte k = (byte) j;
        System.out.println(k);
    }
}
