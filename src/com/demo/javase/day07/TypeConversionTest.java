package com.demo.javase.day07;

/**
 * 自动装箱、自动拆箱等综合测试
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:42
 */
public class TypeConversionTest {

    public static void main(String[] args) {
        // 自动类型转换
        byte b = 10;
        int i = b;
        System.out.println(i);

        // 强制类型转换
        int j = 100;
        byte k = (byte) j;
        System.out.println(k);

        // 包装类
        Integer x = new Integer(100);
        System.out.println(x.intValue());

        // 自动装箱和自动拆箱
        Integer y = 100;
        int z = y;
        System.out.println(y.intValue());
        System.out.println(z);
    }
}


