package com.demo.javase.day07;

/**
 * 包装类
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:38
 */
public class WrapperClassExample {

    public static void main(String[] args) {
        Integer i = new Integer(100);
        System.out.println(i.intValue());
    }
}
