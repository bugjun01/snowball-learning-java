package com.demo.javase.day08;

/**
 * 演示Java如何定义常量
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:58
 */
public class JavaConstant {

    public static void main(String[] args) {

        final int MAX_NUMBER = 100; // 声明一个名称为 MAX_NUMBER 的常量，值为 100

        System.out.println(MAX_NUMBER);

        // MAX_NUMBER = 200; // 编译错误，常量不能更改
    }
}
