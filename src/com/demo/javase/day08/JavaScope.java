package com.demo.javase.day08;

/**
 * 演示Java的作用域
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 18:00
 */
public class JavaScope {

    public static void main(String[] args) {
        int number = 10; // 声明一个整数类型的变量
        if (number == 10) { // 在 if 代码块中声明一个字符串类型的变量
            String message = "Number is equal to 10!"; // 声明一个字符串类型的变量
            System.out.println(message); // 输出 message 的值
        }
        // 在 if 代码块之外，无法访问 message 变量
        // System.out.println(message); // 编译错误
    }
}
