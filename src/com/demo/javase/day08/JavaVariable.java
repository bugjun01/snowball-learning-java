package com.demo.javase.day08;

/**
 * 演示Java如何定义变量
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 17:56
 */
public class JavaVariable {

    public static void main(String[] args) {
        int number_1;  // 声明一个整数类型的变量
        number_1 = 10; // 定义一个整数为 10 的变量
        System.out.println(number_1); // 输出变量值，结果为10


        int number_2 = 10;  // 声明一个整数为 10 的变量
        number_2 = 20;// 更新变量的值

        System.out.println(number_2); // 输出变量值，结果为20
    }
}
