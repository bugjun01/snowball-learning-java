package com.demo.javase.day09;

/**
 * Java之算术运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 22:34
 */
public class ArithmeticOperatorTest {

    public static void test() {
        int a = 5;
        int b = 2;
        int sum = a + b;    // 加法运算符
        int diff = a - b;   // 减法运算符
        int product = a * b;// 乘法运算符
        int quotient = a / b;// 除法运算符
        int remainder = a % b;// 取模运算符
        System.out.println("a + b = " + sum);
        System.out.println("a - b = " + diff);
        System.out.println("a * b = " + product);
        System.out.println("a / b = " + quotient);
        System.out.println("a % b = " + remainder);
    }

    public static void main(String[] args) {
        test();
    }


}
