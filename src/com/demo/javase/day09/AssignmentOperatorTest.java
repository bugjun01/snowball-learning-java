package com.demo.javase.day09;

/**
 * Java之赋值运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 23:04
 */
public class AssignmentOperatorTest {

    public static void main(String[] args) {
        int a = 10;
        int b = 5;

        // 使用赋值运算符将b的值赋给a
        a = b;

        System.out.println("a = " + a); // 输出结果为：a = 5

        // 使用+=运算符
        a += 3;

        System.out.println("a = " + a); // 输出结果为：a = 8

        // 使用-=运算符
        a -= 2;

        System.out.println("a = " + a); // 输出结果为：a = 6

        // 使用*=运算符
        a *= 2;

        System.out.println("a = " + a); // 输出结果为：a = 12

        // 使用/=运算符
        a /= 4;

        System.out.println("a = " + a); // 输出结果为：a = 3

        // 使用%=运算符
        a %= 2;

        System.out.println("a = " + a); // 输出结果为：a = 1
    }
}
