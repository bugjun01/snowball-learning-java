package com.demo.javase.day09;

/**
 * Java之位运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 22:59
 */
public class BitwiseOperatorsTest {

    public static void main(String[] args) {
        int a = 60; // 二进制表示为0011 1100
        int b = 13; // 二进制表示为0000 1101
        int c = a & b; // 按位与操作
        System.out.println("a & b = " + c); // 输出：a & b = 12

        int d = a | b; // 按位或操作
        System.out.println("a | b = " + d); // 输出：a | b = 61

        int e = a ^ b; // 按位异或操作
        System.out.println("a ^ b = " + e); // 输出：a ^ b = 49

        int f = ~a; // 按位取反操作
        System.out.println("~a = " + f); // 输出：~a = -61

        int g = a << 2; // 左移位操作
        System.out.println("a << 2 = " + g); // 输出：a << 2 = 240

        int h = a >> 2; // 右移位操作
        System.out.println("a >> 2 = " + h); // 输出：a >> 2 = 15

        int i = -60; // 二进制表示为1111 1111 1111 1111 1111 1111 1100 0100
        int j = i >>> 2; // 无符号右移位操作
        System.out.println("i >>> 2 = " + j); // 输出：i >>> 2 = 1073741821
    }
}
