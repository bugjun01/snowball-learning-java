package com.demo.javase.day09;

/**
 * Java之关系运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 22:43
 */
public class CompareOperatorTest {
    public static void main(String[] args) {
        int a = 10;
        int b = 5;
        String c = "Hello";
        String d = "hello";
        boolean result1 = (a == b);
        boolean result2 = (a != b);
        boolean result3 = (a > b);
        boolean result4 = (a >= b);
        boolean result5 = (a < b);
        boolean result6 = (a <= b);
        boolean result7 = (c.equals(d));
        boolean result8 = (c.equalsIgnoreCase(d));
        System.out.println("a == b is " + result1);
        System.out.println("a != b is " + result2);
        System.out.println("a > b is " + result3);
        System.out.println("a >= b is " + result4);
        System.out.println("a < b is " + result5);
        System.out.println("a <= b is " + result6);
        System.out.println("c.equals(d) is " + result7);
        System.out.println("c.equalsIgnoreCase(d) is " + result8);
    }
}
