package com.demo.javase.day09;

/**
 * Java之条件运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 23:11
 */
public class ConditionOperatorTest {


    public static int max(int a, int b) {
        return (a > b) ? a : b;
    }

    public static boolean isEven(int a) {
        return (a % 2 == 0) ? true : false;
    }

    public static int checkRange(int x, int min, int max) {
        return (x >= min && x <= max) ? x : -1;
    }

    public static boolean isNullOrEmpty(String str) {
        return (str != null && !str.equals("")) ? false : true;
    }

    public static void main(String[] args) {
        // 测试1：判断两个数的大小
        System.out.println("Max=" + ConditionOperatorTest.max(10, 20));

        // 测试2：判断一个数是否为偶数
        System.out.println("10是偶数吗？" + ConditionOperatorTest.isEven(10));

        // 测试3：判断一个数是否在指定范围内
        System.out.println("20在10-30的范围内吗？" + ConditionOperatorTest.checkRange(20, 10, 30));

        // 测试4：判断一个字符串是否为空
        System.out.println("\"Hello World\"是否为空字符串？" + ConditionOperatorTest.isNullOrEmpty("Hello World"));
    }
}
