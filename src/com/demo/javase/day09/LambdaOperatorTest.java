package com.demo.javase.day09;

import java.util.Arrays;
import java.util.List;

/**
 * Java之Lambda运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 23:38
 */
public class LambdaOperatorTest {

    public static void main(String[] args) {
        List<String> list = Arrays.asList("apple", "banana", "orange", "peach");
        list.sort((s1, s2) -> s1.compareTo(s2));

        System.out.println(list);
    }
}
