package com.demo.javase.day09;

/**
 * Java之逻辑运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 22:48
 */
public class LogicalOperatorsTest {
    public static void main(String[] args) {
        int a = 10;
        int b = 20;
        int c = 30;

        // 逻辑与(&&)
        if (a > 0 && b > 0) {
            System.out.println("a和b都大于0");
        } else {
            System.out.println("a和b中至少有一个小于等于0");
        }

        // 逻辑或(||)
        if (b > 0 || c > 0) {
            System.out.println("b和c中至少有一个大于0");
        } else {
            System.out.println("b和c都小于等于0");
        }

        // 逻辑非(!)
        if (!(a > 0)) {
            System.out.println("a小于等于0");
        } else {
            System.out.println("a大于0");
        }
    }

}
