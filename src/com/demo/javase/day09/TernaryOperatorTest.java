package com.demo.javase.day09;

/**
 * Java之三目运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 23:30
 */
public class TernaryOperatorTest {

    public static void test1(){
        int a = 1;
        int b = 2;
        int max = (a > b) ? a : b;
        System.out.println("(a > b) ? a : b ->"+max);
    }

    public static void test2(){
        int num = 6;
        String type = (num % 2 == 0) ? "even" : "odd";
        System.out.println("(num % 2 == 0) ? \"even\" : \"odd\" ->" + type);
    }

    public static void test3(){
        int num = 5;
        String type = (num == 0) ? "zero" : "non-zero";
        System.out.println("(num == 0) ? \"zero\" : \"non-zero\" ->" + type);
    }

    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }
}
