package com.demo.javase.day09;

/**
 * Java之单目运算符案例演示
 *
 * @Author bug菌
 * @Date 2023-11-08 23:24
 */
public class UnaryOperatorTest {

    public static void main(String[] args) {
        int a = 5, b = -3;
        System.out.println("a=" + a + ", b=" + b);
        a++;
        System.out.println("a++=" + a);
        b--;
        System.out.println("b--=" + b);
        int c = ++a;
        System.out.println("++a=" + c);
        int d = b--;
        System.out.println("b--=" + d);
        int e = +a;
        System.out.println("+a=" + e);
        int f = -b;
        System.out.println("-b=" + f);
        boolean g = true;
        boolean h = !g;
        System.out.println("!g=" + h);
    }
}
