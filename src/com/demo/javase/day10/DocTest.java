package com.demo.javase.day10;

/**
 * @Author bug菌
 * @Date 2023-12-27 14:41
 */
public class DocTest {

    public static class Calculator {

        /**
         * 计算两个数的和
         *
         * @param x 第一个操作数
         * @param y 第二个操作数
         * @return 两个数的和
         */
        public int add(int x, int y) {
            return x + y;
        }
    }

    /**
     * 主函数
     */
    public static void main(String[] args) {
        // 创建计算器对象
        Calculator calculator = new Calculator();
        // 计算两个数的和
        int sum = calculator.add(2, 3);
        // 打印和
        System.out.println("2 + 3 = " + sum);
    }
}
