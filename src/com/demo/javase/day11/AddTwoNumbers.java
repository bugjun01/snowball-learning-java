package com.demo.javase.day11;

import java.util.Scanner;

/**
 * @Author bug菌
 * @Date 2023-12-27 14:59
 */
public class AddTwoNumbers {

    public static void main(String[] args) {
        int firstNumber, secondNumber, sum;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the first number: ");
        firstNumber = scanner.nextInt();

        System.out.println("Enter the second number: ");
        secondNumber = scanner.nextInt();

        sum = firstNumber + secondNumber;

        System.out.println("The sum of " + firstNumber + " and " + secondNumber + " is " + sum);
    }
}
