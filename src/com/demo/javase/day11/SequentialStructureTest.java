package com.demo.javase.day11;

/**
 * @Author bug菌
 * @Date 2023-12-27 14:57
 */
public class SequentialStructureTest {

    public static void main(String[] args) {
        String str1 = "Hello,";
        String str2 = " World!";
        String result = str1 + str2;
        System.out.println(result);
    }

}
