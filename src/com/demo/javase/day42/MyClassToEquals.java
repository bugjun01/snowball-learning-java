package com.demo.javase.day42;

/**
 * 实现equals()方法的例子
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 15:09
 */
public class MyClassToEquals {

    private int x;
    private int y;

    //重写equals()
    public boolean equals(Object o) {
        if (!(o instanceof MyClassToEquals)) { // 检查o是否是MyClassToEquals类型
            return false;
        }
        MyClassToEquals obj = (MyClassToEquals) o; // 强制转换为MyClassToEquals类型
        return obj.x == x && obj.y == y; // 比较x和y
    }

    public static void main(String[] args) {

        MyClassToEquals myClassToEquals_1 = new MyClassToEquals();
        MyClassToEquals myClassToEquals_2 = new MyClassToEquals();
        myClassToEquals_1.x = 1;
        myClassToEquals_1.y = 2;

        myClassToEquals_2.x = 1;
        myClassToEquals_2.y = 2;

        System.out.println(myClassToEquals_1.equals(myClassToEquals_2));
    }

}
