package com.demo.javase.day42;

/**
 * 实现hashCode()方法的例子
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 14:54
 */
public class MyClassToHashCode {

    private int x;
    private int y;

    //重写hashCode方法
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    public static void main(String[] args) {
        MyClassToHashCode myClassToHashCode = new MyClassToHashCode();
        myClassToHashCode.x = 1;
        myClassToHashCode.y = 2;
        System.out.println(myClassToHashCode.hashCode());
    }
}
