package com.demo.javase.day42;

/**
 * 实现toString()方法的例子
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 14:55
 */
public class MyClassToString {

    private int x;
    private int y;

    //重写toString方法
    public String toString() {
        return "MyClass [x=" + x + ", y=" + y + "]";
    }

    public static void main(String[] args) {

        MyClassToString myClassToString = new MyClassToString();
        myClassToString.x = 1;
        myClassToString.y = 2;
        System.out.println(myClassToString.toString());
    }
}
