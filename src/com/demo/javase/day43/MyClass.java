package com.demo.javase.day43;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 演示Class类的各种方法
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 15:21
 */
public class MyClass {

    private int mValue;

    public MyClass(int value) {
        mValue = value;
    }

    public int getValue() {
        return mValue;
    }

    public static void main(String[] args) {
        // 获取 Class 对象
        Class clazz = MyClass.class;

        // 获取类的名称
        String className = clazz.getName();
        System.out.println(className);

        // 获取类的父类
        Class superClass = clazz.getSuperclass();
        System.out.println(superClass.getName());

        // 获取类的接口
        Class[] interfaces = clazz.getInterfaces();
        for (Class i : interfaces) {
            System.out.println(i.getName());
        }

        // 获取类的成员变量
        Field[] fields = clazz.getDeclaredFields();
        for (Field f : fields) {
            System.out.println(f.getName());
        }

        // 获取类的方法
        Method[] methods = clazz.getDeclaredMethods();
        for (Method m : methods) {
            System.out.println(m.getName());
        }
    }

}
