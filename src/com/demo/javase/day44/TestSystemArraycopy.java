package com.demo.javase.day44;

import java.util.Arrays;

/**
 * 演示 System.arraycopy(Object src, int srcPos, Object dest, int destPos, int length)
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 10:52
 */
public class TestSystemArraycopy {

    //将数组src中从索引srcPos开始的length个元素复制到数组dest中的索引destPos开始的位置
    public static void main(String[] args) {
        int[] src = {1, 2, 3, 4, 5};
        int[] dest = new int[5];
        System.arraycopy(src, 0, dest, 0, 5);
        System.out.println(Arrays.toString(dest));
    }
}
