package com.demo.javase.day44;

/**
 * 演示 System.currentTimeMillis()
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 10:47
 */
public class TestSystemCurrentTimeMillis {

    //获取系统当前的时间戳
    public static void main(String[] args) throws InterruptedException {
        long startTime = System.currentTimeMillis();
        System.out.println("开始时间为：" + startTime);

        // 执行一些耗时的操作（这里线程睡眠5s作为模拟）
        Thread.sleep(5000);

        long endTime = System.currentTimeMillis();
        System.out.println("结束时间为：" + endTime);
        System.out.println("程序运行时间：" + (endTime - startTime) + "ms");
    }
}
