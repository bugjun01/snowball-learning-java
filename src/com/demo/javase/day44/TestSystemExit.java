package com.demo.javase.day44;

/**
 * 演示 System.exit(int status)
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 10:39
 */
public class TestSystemExit {

    //终止Java虚拟机，并返回一个指定的状态码
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.exit(0);
        System.out.println("Hello World!!!"); //不会被执行(控制台不会输出)
    }
}
