package com.demo.javase.day44;

import java.util.Enumeration;
import java.util.Properties;

/**
 * 演示 System.getProperties()方法
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 11:00
 */
public class TestSystemGetProperties {

    //返回一个Properties对象，包含了当前Java虚拟机的系统属性
    public static void main(String[] args) {
        Properties props = System.getProperties();
        Enumeration<?> e = props.propertyNames();
        while (e.hasMoreElements()) {
            String key = (String) e.nextElement();
            String value = (String) props.get(key);
            System.out.println(key + " = " + value);
        }
    }
}
