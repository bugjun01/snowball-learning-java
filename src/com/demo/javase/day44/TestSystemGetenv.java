package com.demo.javase.day44;

/**
 * 演示 System.getenv(String name)
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 10:55
 */
public class TestSystemGetenv {

    //获取系统环境变量name的值
    public static void main(String[] args) {
        String javaHome = System.getenv("JAVA_HOME");
        System.out.println("JAVA_HOME：" + javaHome);

        System.out.println("CLASSPATH："+System.getenv("CLASSPATH"));
    }
}
