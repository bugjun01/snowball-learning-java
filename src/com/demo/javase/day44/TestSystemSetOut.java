package com.demo.javase.day44;

import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 11:02
 */
public class TestSystemSetOut {

    //该方法用于将System.out设置为指定的输出流out
    public static void main(String[] args) {
        try {
            PrintStream out = new PrintStream(new FileOutputStream("output.txt"));
            System.setOut(out);
            System.out.println("Hello World!");
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
