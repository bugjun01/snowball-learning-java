package com.demo.javase.day45;

import java.io.IOException;

/**
 * 演示 Java-Runtime类
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 14:14
 */
public class TestRuntime {

    public static void main(String[] args) throws IOException {
        // 返回JVM可用的处理器数目
        int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("处理器数目：" + processors);

        // 启动记事本编辑器
        Process process = Runtime.getRuntime().exec("notepad.exe");

        // 返回JVM当前空闲的内存量
        long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("空闲内存：" + freeMemory);

        // 返回JVM当前总共可用的内存量
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("总共可用内存：" + totalMemory);

        // 强制执行垃圾回收
        Runtime.getRuntime().gc();

        // 强制结束JVM的运行
        Runtime.getRuntime().exit(0);

        // 注册一个线程，在JVM关闭时执行
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            System.out.println("JVM正在关闭...");
        }));
    }
}
