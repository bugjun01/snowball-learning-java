package com.demo.javase.day46;

/**
 * 演示Java中Math类
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 14:42
 */
public class TestMath {

    public static void main(String[] args) {
        double a = -10.5;
        double b = 5.5;
        double c = 2.0;

        // abs方法
        System.out.println(Math.abs(a)); // 输出：10.5

        // pow方法
        System.out.println(Math.pow(b, c)); // 输出：30.25

        // sqrt方法
        System.out.println(Math.sqrt(b)); // 输出：2.345207879911715

        // max方法
        System.out.println(Math.max(a, b)); // 输出：5.5

        // min方法
        System.out.println(Math.min(a, b)); // 输出：-10.5

        // ceil方法
        System.out.println(Math.ceil(a)); // 输出：-10.0

        // floor方法
        System.out.println(Math.floor(a)); // 输出：-11.0

        // round方法
        System.out.println(Math.round(b)); // 输出：6

        // random方法
        System.out.println(Math.random()); // 输出：0.40177626192565223
    }
}
