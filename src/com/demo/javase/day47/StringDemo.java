package com.demo.javase.day47;

/**
 * @author bug菌
 * @version 1.0
 * @date 2023/10/18 15:35
 */
public class StringDemo {

    //替换字符串
    public static void testReplace() {
        String str1 = "Hello World";
        String str2 = str1.replace("World", "Java"); // "Hello Java"
        String str3 = str1.replaceAll("o", "O"); // "HellO WOrld"

        System.out.println(str2);
        System.out.println(str3);
    }

    //分割字符串
    public static void testSplit() {
        String str1 = "Hello,World,Java";
        String[] str2 = str1.split(","); // {"Hello", "World", "Java"}

        for (String str : str2) {
            System.out.println(str);
        }
    }

    //字符串搜索
    public static void testIndexOf() {
        String str = "Hello World";
        int index1 = str.indexOf("o");         // 4
        int index2 = str.lastIndexOf("o");     // 7
        System.out.println("index1:" + index1);
        System.out.println("index2:" + index2);
    }

    public static void main(String[] args) {
        testReplace();
    }
}
