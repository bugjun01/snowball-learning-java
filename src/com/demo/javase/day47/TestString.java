package com.demo.javase.day47;

/**
 * String类演示
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 14:53
 */
public class TestString {

    public static void main(String[] args) {
        String str1 = "Hello";
        String str2 = "hello";

        // test string equality with case sensitive
        if (str1.equals(str2)) {
            System.out.println("The two strings are equal.");
        } else {
            System.out.println("The two strings are not equal.");
        }

        // test string equality with case insensitive
        if (str1.compareToIgnoreCase(str2) == 0) {
            System.out.println("The two strings are equal.");
        } else {
            System.out.println("The two strings are not equal.");
        }

        // test substring
        String str = "Hello World";
        String str3 = str.substring(0, 5);
        String str4 = str.substring(6);
        System.out.println(str3 + " " + str4);

        // test string length
        int len = str.length();
        System.out.println(len);

        // test string concatenation
        String str5 = "Java";
        String str6 = str1 + " " + str5;
        System.out.println(str6);

        // test string replace
        String newStr = str.replace("World", "Java");
        System.out.println(newStr);
    }

}
