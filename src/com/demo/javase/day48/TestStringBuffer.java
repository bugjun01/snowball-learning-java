package com.demo.javase.day48;

/**
 * 演示 StringBuffer类
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/10 15:16
 */
public class TestStringBuffer {

    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer();
        sb.append("Hello");
        sb.append(" ");
        sb.append("World");
        System.out.println(sb.toString()); // 输出 "Hello World"

        sb.insert(5, ",");
        System.out.println(sb.toString()); // 输出 "Hello, World"

        sb.delete(5, 6);
        System.out.println(sb.toString()); // 输出 "Hello World"

        sb.replace(6, 11, "Java");
        System.out.println(sb.toString()); // 输出 "Hello Java"

        sb.reverse();
        System.out.println(sb.toString()); // 输出 "avaJ olleH"
    }
}
