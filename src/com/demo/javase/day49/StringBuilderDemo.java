package com.demo.javase.day49;

/**
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 10:50
 */
public class StringBuilderDemo {

    //定义一个用于拼接字符串的方法
    public static String concat(String... strings) {
        StringBuilder sb = new StringBuilder();
        for (String s : strings) {
            sb.append(s);
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String str = StringBuilderDemo.concat("hello", "world");
        System.out.println(str);
    }

}
