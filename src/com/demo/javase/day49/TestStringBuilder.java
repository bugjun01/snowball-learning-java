package com.demo.javase.day49;

/**
 * 演示StringBuilder类
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 10:38
 */
public class TestStringBuilder {


    //append()方法
    public static void testAppend() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello");
        sb.append("world");
        System.out.println(sb);
    }

    //insert()方法
    public static void testInsert() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello");
        sb.insert(0, "world");
        System.out.println(sb);
    }

    //delete()方法
    public static void testDelete() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello");
        sb.delete(1, 3); // 将删除e和l字符
        System.out.println(sb);
    }

    //replace()方法
    public static void testReplace() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello");
        sb.replace(1, 3, "w"); // 将替换e和l字符为w
        System.out.println(sb);
    }

    //reverse()方法
    public static void testReverse() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello");
        sb.reverse(); // 输出olleh
        System.out.println(sb);
    }

    //链式调用
    public static void testMethodChaining() {
        StringBuilder sb = new StringBuilder();
        sb.append("hello").append("world").insert(0, "my "); // 实现了多个操作
        System.out.println(sb);
    }

    public static void main(String[] args) {
//        TestStringBuilder.testAppend();
//        TestStringBuilder.testInsert();
//        TestStringBuilder.testDelete();
//        TestStringBuilder.testReplace();
//        TestStringBuilder.testReverse();
        TestStringBuilder.testMethodChaining();
    }
}
