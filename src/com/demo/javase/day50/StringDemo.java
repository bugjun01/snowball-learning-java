package com.demo.javase.day50;

/**
 * @author bug菌
 * @version 1.0
 * @date 2023/10/12 11:17
 */
public class StringDemo {

    public static void main(String[] args) {
        String s = "hello";
        s = s + "world"; // 新对象
        System.out.println(s);

        StringBuilder sb = new StringBuilder("hello");
        sb.append("world"); // 相同对象
        System.out.println(sb.toString());

        StringBuffer sbf = new StringBuffer("hello");
        sbf.append("world"); // 相同对象
        System.out.println(sbf.toString());
    }
}
