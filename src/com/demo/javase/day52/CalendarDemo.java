package com.demo.javase.day52;

import java.util.Calendar;

/**
 * @author bug菌
 * @date 2023/10/17 10:44
 */
public class CalendarDemo {

    public static void main(String[] args) {
        Calendar now = Calendar.getInstance();
        System.out.println("当前时间为：" + now.getTime());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2023);
        calendar.set(Calendar.MONTH, 10);
        calendar.set(Calendar.DATE, 17);
        System.out.println("设置时间为：" + calendar.getTime());

        calendar.add(Calendar.DATE, 10);
        System.out.println("加上10天后的时间为：" + calendar.getTime());

        calendar.add(Calendar.MONTH, -2);
        System.out.println("减去2个月后的时间为：" + calendar.getTime());

        calendar.add(Calendar.YEAR, 3);
        System.out.println("加上3年后的时间为：" + calendar.getTime());

        System.out.println("年份为：" + calendar.get(Calendar.YEAR));
        System.out.println("月份为：" + calendar.get(Calendar.MONTH));
        System.out.println("日期为：" + calendar.get(Calendar.DATE));
    }
}
