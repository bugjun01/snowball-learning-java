package com.demo.javase.day52;

import java.util.Calendar;

/**
 * @author bug菌
 * @date 2023/10/17 10:44
 */
public class CalendarTest {

    //获取当前日期
    public static void getNowDate() {
        Calendar now = Calendar.getInstance();
        System.out.println("当前时间为：" + now.getTime());
    }

    //设置日期
    public static void setDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2023);
        calendar.set(Calendar.MONTH, 10);
        calendar.set(Calendar.DATE, 17);
        System.out.println(calendar.getTime());
    }

    //日期计算
    public static void CalculateDate() {
        Calendar calendar = Calendar.getInstance();
        System.out.println("当前时间为：" + calendar.getTime());

        calendar.add(Calendar.DATE, 10);
        System.out.println("加上10天后的时间为：" + calendar.getTime());

        calendar.add(Calendar.MONTH, -2);
        System.out.println("减去2个月后的时间为：" + calendar.getTime());

        calendar.add(Calendar.YEAR, 3);
        System.out.println("加上3年后的时间为：" + calendar.getTime());
    }


    public static void main(String[] args) {
        CalculateDate();
    }


}
