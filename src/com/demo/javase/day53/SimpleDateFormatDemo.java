package com.demo.javase.day53;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author bug菌
 * @date 2023/10/17 19:17
 */
public class SimpleDateFormatDemo {

    public static void main(String[] args) throws Exception {
        // 格式化日期时间
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr1 = sdf1.format(new Date());
        System.out.println(dateStr1);

        // 解析日期时间
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        Date date2 = sdf2.parse("2023-10-17");
        System.out.println(date2);

        // 设置时区
        SimpleDateFormat sdf3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf3.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        String dateStr3 = sdf3.format(new Date());
        System.out.println(dateStr3);

        // 转义字符
        SimpleDateFormat sdf4 = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        String dateStr4 = sdf4.format(new Date());
        System.out.println(dateStr4);

        // 数字格式化
        SimpleDateFormat sdf5 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
        String dateStr5 = sdf5.format(new Date());
        System.out.println(dateStr5);

        // 格式化模式
        SimpleDateFormat sdf6 = new SimpleDateFormat("yyyy-MM-dd E HH:mm:ss");
        String dateStr6 = sdf6.format(new Date());
        System.out.println(dateStr6);
    }
}
