package com.demo.javase.day53;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author bug菌
 * @date 2023/10/17 19:17
 */
public class SimpleDateFormatTest {

    //格式化日期时间
    public static void testFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = sdf.format(new Date());
        System.out.println(dateStr);
    }

    //解析日期时间
    public static void testParse() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse("2023-10-17");
        System.out.println(date);
    }

    //设置区时
    public static void testSetTimeZone() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+8"));
        String dateStr = sdf.format(new Date());
        System.out.println(dateStr);
    }

    //转义字符
    public static void testSimpleDateFormat() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        String dateStr = sdf.format(new Date());
        System.out.println(dateStr);
    }

    //数字格式化
    public static void testSimpleDateFormat_1() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String dateStr = sdf.format(new Date());
        System.out.println(dateStr);
    }

    //格式化模式
    public static void testSimpleDateFormat_2() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd E HH:mm:ss");
        String dateStr = sdf.format(new Date());
        System.out.println(dateStr);
    }

    public static void main(String[] args) throws ParseException {
        testSimpleDateFormat_2();
    }

}
