package com.demo.javase.day54;

/**
 * 案例3(计算人体重指数)
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/18 17:12
 */
public class BMI {

    private double height;
    private double weight;

    public BMI(double height, double weight) {
        this.height = height;
        this.weight = weight;
    }

    //BMI体质指数的计算公式:体重(公斤)除以身高(米)的平方。
    public double getBMI() {
        return weight / Math.pow(height, 2);
    }

    public static void main(String[] args) {
        BMI bmi = new BMI(1.72,68);
        double bmi1 = bmi.getBMI();
        System.out.println("BMI:"+bmi1);
    }

}
