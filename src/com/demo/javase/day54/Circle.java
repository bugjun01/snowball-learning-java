package com.demo.javase.day54;

/**
 * 案例1(计算圆面积和周长)
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/18 17:02
 */
public class Circle {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    //计算圆面积(公式：PI*r^2)
    public double getArea() {
        return Math.PI * Math.pow(r, 2);
    }

    //计算圆周长(公式：2*PI*r)
    public double getPerimeter() {
        return 2 * Math.PI * r;
    }

    public static void main(String[] args) {
        Circle circle = new Circle(3);
        double area = circle.getArea();
        double perimeter = circle.getPerimeter();
        System.out.println("圆的面积为：" + area);
        System.out.println("圆的周长为：" + perimeter);
    }
}
