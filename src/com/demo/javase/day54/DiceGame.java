package com.demo.javase.day54;

import java.util.Random;

/**
 * 案例2(实现骰子游戏)
 *
 * @author bug菌
 * @version 1.0
 * @date 2023/10/18 17:07
 */
public class DiceGame {

    public static void main(String[] args) {
        Random random = new Random();
        int result = random.nextInt(6) + 1;
        System.out.println("骰子点数为：" + result);
    }
}
