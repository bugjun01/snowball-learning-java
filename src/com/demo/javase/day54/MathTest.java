package com.demo.javase.day54;

/**
 * @author bug菌
 * @version 1.0
 * @date 2023/10/18 16:51
 */
public class MathTest {

    //计算绝对值
    public static void testAbs() {
        int abs = Math.abs(-5);
        System.out.println("-5的绝对值为：" + abs);
    }

    //计算两个数的最大值
    public static void testMax() {
        int max = Math.max(4, 5);
        System.out.println("4与5谁更大：" + max);
    }

    //计算两个数的最小值
    public static void testMin() {
        int min = Math.min(-3, 7);
        System.out.println("-3与7谁更小：" + min);
    }

    //向上取整
    public static void testCeil() {
        double ceil = Math.ceil(3.4);
        System.out.println("3.4向上取整为：" + ceil);
    }

    //向下取整
    public static void testFloor() {
        double floor = Math.floor(7.5);
        System.out.println("7.5向下取整为：" + floor);
    }

    public static void main(String[] args) {
        testAbs();
        testMax();
        testMin();
        testCeil();
        testFloor();
    }
}
