package com.demo.javase.day55;

import java.util.Random;

/**
 * @author bug菌
 * @version 1.0
 * @date 2023/10/18 17:55
 */
public class RandomDemo {

    public static void main(String[] args) {
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            System.out.print(random.nextInt(100)+",");
        }
    }

}
