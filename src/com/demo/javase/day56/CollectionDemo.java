package com.demo.javase.day56;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author bug菌
 * @Date 2023-10-23 20:14
 */
public class CollectionDemo {

    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();

        // 添加数据
        collection.add("赵云");
        collection.add("张飞");
        collection.add("刘备");

        // 判断数据是否存在
        System.out.println(collection.contains("刘备"));   // true
        System.out.println(collection.contains("貂蝉"));    // false

        // 输出数据
        System.out.println(collection);    // [赵云, 张飞, 刘备]

        // 删除数据
        collection.remove("张飞");

        // 输出数据
        System.out.println("删除后："+collection);    // [赵云, 刘备]

        // 统计数据
        System.out.println("统计数据条数："+collection.size());    // 2
    }


}
