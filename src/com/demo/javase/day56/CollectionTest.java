package com.demo.javase.day56;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author bug菌
 * @Date 2023-10-23 20:14
 */
public class CollectionTest {


    public static void main(String[] args) {
        Collection<String> collection = new ArrayList<>();

        // 添加数据
        collection.add("Java");
        collection.add("Python");
        collection.add("C++");

        // 判断数据是否存在
        System.out.println(collection.contains("Java"));
        System.out.println(collection.contains("Golang"));

        // 输出数据
        System.out.println(collection);    // [Java, Python, C++]

        // 删除数据
        collection.remove("C++");

        // 输出数据
        System.out.println(collection);    // [Java, Python]

        // 统计数据
        System.out.println(collection.size());    // 2
    }
}
