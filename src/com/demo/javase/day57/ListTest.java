package com.demo.javase.day57;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author bug菌
 * @Date 2023-11-05 23:32
 */
public class ListTest {
        public static void main(String[] args) {

            // 创建一个列表
            List<String> list = new ArrayList<>();

            // 添加元素到列表
            list.add("A");
            list.add("B");
            list.add("C");

            // 输出列表长度
            System.out.println("Size of list: " + list.size());

            // 输出列表中的元素
            System.out.println("List contents: ");
            for (String s : list) {
                System.out.println(s);
            }

            // 删除第一个元素
            list.remove(0);

            // 输出修改后的列表中的元素
            System.out.println("List contents after removing first element: ");
            for (String s : list) {
                System.out.println(s);
            }

            // 判断列表中是否包含指定元素
            if (list.contains("A")) {
                System.out.println("List contains A");
            } else {
                System.out.println("List does not contain A");
            }

            // 清空列表
            list.clear();

            // 输出清空后的列表长度
            System.out.println("Size of list after clearing: " + list.size());
        }

}
