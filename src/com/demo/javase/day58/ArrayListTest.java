package com.demo.javase.day58;

import java.util.ArrayList;

/**
 * @Author bug菌
 * @Date 2023-11-05 23:45
 */
public class ArrayListTest {

        public static void main(String[] args) {
            // 创建一个空的ArrayList对象
            ArrayList<String> list = new ArrayList<String>();

            // 添加元素
            list.add("Java");
            list.add("Python");
            list.add("C++");

            // 遍历元素
            for (String element : list) {
                System.out.println(element);
            }

            // 插入元素
            list.add(1, "JavaScript");

            // 删除元素
            list.remove("C++");

            // 查找元素
            boolean contains = list.contains("Java");
            int index = list.indexOf("Python");

            // 输出结果
            System.out.println(list);
            System.out.println("List contains Java? " + contains);
            System.out.println("Index of Python: " + index);
        }
    }
