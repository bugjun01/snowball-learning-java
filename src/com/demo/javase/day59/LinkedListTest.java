package com.demo.javase.day59;

import java.util.LinkedList;

/**
 * @Author bug菌
 * @Date 2023-11-05 23:48
 */
public class LinkedListTest {

    public static void main(String[] args) {
        LinkedList<String> linkedList = new LinkedList<String>();

        // 添加元素到LinkedList
        linkedList.add("A");
        linkedList.add("B");
        linkedList.add("C");

        // 打印LinkedList中的元素
        System.out.println(linkedList);

        // 在LinkedList的开头和末尾添加元素
        linkedList.addFirst("D");
        linkedList.addLast("E");

        // 打印LinkedList中的元素
        System.out.println(linkedList);

        // 删除LinkedList中的第一个和最后一个元素
        linkedList.removeFirst();
        linkedList.removeLast();

        // 打印LinkedList中的元素
        System.out.println(linkedList);

        // 获取LinkedList中的元素数量
        int size = linkedList.size();
        System.out.println("size: " + size);

        // 根据下标获取LinkedList中指定的元素
        String element = linkedList.get(1);
        System.out.println("element: " + element);

        // 替换LinkedList中指定下标的元素
        linkedList.set(1, "F");

        // 打印LinkedList中的元素
        System.out.println(linkedList);
    }
}
