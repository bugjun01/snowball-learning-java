package com.demo.javase.day60;

import java.util.Vector;

/**
 * @Author bug菌
 * @Date 2023-11-05 23:56
 */
public class VectorTest {

    public static void main(String[] args) {

        Vector<String> vector = new Vector<String>();
        //添加元素到Vector中
        vector.add("Java");
        vector.add("Python");
        vector.add("C++");
        System.out.println("元素个数：" + vector.size());
        //获取Vector中的元素
        System.out.println("第一个元素：" + vector.get(0));
        System.out.println("第二个元素：" + vector.get(1));
        System.out.println("第三个元素：" + vector.get(2));
        //修改Vector中的元素
        vector.set(0, "Java SE");
        System.out.println("修改后的第一个元素：" + vector.get(0));
        //删除Vector中的元素
        vector.remove(2);
        System.out.println("删除后的元素个数：" + vector.size());
    }

}
