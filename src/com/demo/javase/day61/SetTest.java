package com.demo.javase.day61;

import sun.reflect.generics.tree.Tree;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

/**
 * @Author bug菌
 * @Date 2023-11-06 10:33
 */
public class SetTest {

    public static void main(String[] args) {

        // 创建一个 HashSet 对象
        Set<String> set = new HashSet<String>();
        // 创建一个 TreeSet 对象
        Set<String> treeSet = new TreeSet<>();

        // 向集合中添加元素
        set.add("Java");
        set.add("C++");
        set.add("Python");

        // 打印出集合中的元素个数
        System.out.println("集合中的元素个数为：" + set.size());

        // 判断集合是否为空
        System.out.println("集合是否为空：" + set.isEmpty());

        // 判断集合中是否包含某个元素
        System.out.println("集合中是否包含 Python：" + set.contains("Python"));

        // 从集合中移除某个元素
        set.remove("C++");
        System.out.println("从集合中移除元素后，集合中的元素个数为：" + set.size());

        // 使用迭代器遍历集合中的元素
        Iterator<String> iterator = set.iterator();
        System.out.println("遍历集合中的元素：");
        while (iterator.hasNext()) {
            String element = iterator.next();
            System.out.println(element);
        }

        // 清空集合中的所有元素
        set.clear();
        System.out.println("清空集合中的元素后，集合中的元素个数为：" + set.size());
    }
}
