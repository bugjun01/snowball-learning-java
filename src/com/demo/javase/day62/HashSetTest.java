package com.demo.javase.day62;

import java.util.HashSet;
import java.util.Iterator;

/**
 * @Author bug菌
 * @Date 2023-11-06 10:54
 */
public class HashSetTest {

    public static void main(String[] args) {
        // 创建一个空的HashSet
        HashSet<String> set = new HashSet<>();

        // 添加元素到HashSet中
        set.add("A");
        set.add("B");
        set.add("C");
        set.add("D");
        set.add("E");

        // 判断HashSet是否包含某个元素
        System.out.println(set.contains("D"));

        // 删除HashSet中的某个元素
        set.remove("B");

        // 获取HashSet的大小
        System.out.println(set.size());

        // 遍历HashSet
        Iterator<String> it = set.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        // 清空HashSet
        set.clear();

        // 判断HashSet是否为空
        System.out.println(set.isEmpty());
    }

}
