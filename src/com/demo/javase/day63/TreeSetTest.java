package com.demo.javase.day63;

import java.util.TreeSet;

/**
 * @Author bug菌
 * @Date 2023-11-06 11:01
 */
public class TreeSetTest {

    public static void main(String[] args) {
        TreeSet<String> set = new TreeSet<>();
        set.add("Java");
        set.add("Python");
        set.add("C++");

        System.out.println(set.contains("Java")); // true
        System.out.println(set.contains("Ruby")); // false

        System.out.println(set.first()); // C++
        System.out.println(set.last()); // Python

        set.remove("Python");
        System.out.println(set.contains("Python")); // false
    }
}
