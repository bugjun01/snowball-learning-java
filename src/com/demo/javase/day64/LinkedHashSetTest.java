package com.demo.javase.day64;

import java.util.LinkedHashSet;

/**
 * @Author bug菌
 * @Date 2023-11-06 11:36
 */
public class LinkedHashSetTest {

    public static void main(String[] args) {
        LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>();

        // 向集合中添加元素
        linkedHashSet.add("A");
        linkedHashSet.add("B");
        linkedHashSet.add("C");
        linkedHashSet.add("D");
        linkedHashSet.add("E");

        // 遍历集合中的元素，并输出它们的顺序
        for (String str : linkedHashSet) {
            System.out.print(str + " ");
        }
        System.out.println();

        // 删除集合中的元素
        linkedHashSet.remove("C");

        // 再次遍历集合中的元素，并输出它们的顺序
        for (String str : linkedHashSet) {
            System.out.print(str + " ");
        }
        System.out.println();

        // 判断集合中是否包含指定元素
        System.out.println("集合中是否包含\"A\"：" + linkedHashSet.contains("A"));
        System.out.println("集合中是否包含\"C\"：" + linkedHashSet.contains("C"));
    }
}
