package com.demo.javase.day65;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author bug菌
 * @Date 2023-11-06 11:45
 */
public class MapTest {
    public static void main(String[] args) {
        // 创建一个HashMap实例
        Map<String, Integer> studentGrades = new HashMap<>();

        // 向Map中添加键值对
        studentGrades.put("张三", 90);
        studentGrades.put("李四", 80);
        studentGrades.put("王五", 70);

        // 判断Map中是否包含某个键
        System.out.println(studentGrades.containsKey("张三"));  // true
        System.out.println(studentGrades.containsKey("赵六"));  // false

        // 获取Map中某个键对应的值
        System.out.println(studentGrades.get("张三"));  // 90

        // 遍历Map中所有的键值对
        for (Map.Entry<String, Integer> entry : studentGrades.entrySet()) {
            System.out.println(entry.getKey() + "的成绩是：" + entry.getValue());
        }

        // 删除Map中的某个键值对
        studentGrades.remove("李四");

        // 清空Map中所有的键值对
        studentGrades.clear();
    }
}
