package com.demo.javase.day65_1;

import java.util.Hashtable;

/**
 * Hashtable示例演示
 *
 * @Author bug菌
 * @Date 2023-11-06 16:53
 */
public class HashtableTest {

    public static void main(String[] args) {
        // 创建Hashtable实例
        Hashtable<String, Integer> hashtable = new Hashtable<String, Integer>();

        // 添加元素
        hashtable.put("Java", 1);
        hashtable.put("Python", 2);
        hashtable.put("C++", 3);

        // 获取元素的值
        System.out.println(hashtable.get("Java")); // 输出：1
        System.out.println(hashtable.get("Python")); // 输出：2
        System.out.println(hashtable.get("C++")); // 输出：3

        // 删除元素
        hashtable.remove("Java");

        // 判断元素是否存在
        System.out.println(hashtable.containsKey("Java")); // 输出：false
        System.out.println(hashtable.containsKey("Python")); // 输出：true
        System.out.println(hashtable.containsValue(2)); // 输出：true

        // 获取Hashtable的大小
        System.out.println(hashtable.size()); // 输出：2
    }

}
