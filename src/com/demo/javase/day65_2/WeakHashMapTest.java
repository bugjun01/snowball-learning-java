package com.demo.javase.day65_2;

import java.util.WeakHashMap;

/**
 * WeakHashMap示例演示
 *
 * @Author bug菌
 * @Date 2023-11-06 16:53
 */
public class WeakHashMapTest {

    public static void main(String[] args) {

        WeakHashMap<String, Object> map = new WeakHashMap<>();
        Object value = new Object();
        map.put("key", value);
        System.out.println(map.containsValue(value)); // true
        value = null;
        System.gc();
        // 等待垃圾回收
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(map.containsValue(value)); // false
    }


}
