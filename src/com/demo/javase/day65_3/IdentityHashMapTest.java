package com.demo.javase.day65_3;

import java.util.IdentityHashMap;

/**
 * IdentityHashMap示例演示
 *
 * @Author bug菌
 * @Date 2023-11-06 16:53
 */
public class IdentityHashMapTest {

    public static void main(String[] args) {
        IdentityHashMap<String, Integer> hashMap = new IdentityHashMap<>();
        String s1 = "hello";
        String s2 = new String("hello");
        hashMap.put(s1, 1);
        hashMap.put(s2, 2);
        System.out.println(hashMap.get(s1)); // 输出 1
        System.out.println(hashMap.get(s2)); // 输出 2
        System.out.println(hashMap.size()); // 输出 2
        hashMap.remove(s1);
        System.out.println(hashMap.size()); // 输出 1
    }
}
