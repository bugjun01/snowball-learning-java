package com.demo.javase.day65_4;

import java.util.concurrent.ConcurrentHashMap;

/**
 * ConcurrentHashMap示例演示
 *
 * @Author bug菌
 * @Date 2023-11-06 16:53
 */
public class ConcurrentHashMapTest {

    public static void main(String[] args) {
        ConcurrentHashMap<String, Integer> map = new ConcurrentHashMap<>();

        // 添加元素
        map.put("one", 1);
        map.put("two", 2);
        map.put("three", 3);

        // 打印元素
        System.out.println(map);

        // 获取元素
        Integer one = map.get("one");
        System.out.println("one = " + one);

        // 移除元素
        Integer removed = map.remove("two");
        System.out.println("removed = " + removed);
        System.out.println(map);

        // 替换元素
        Integer replaced = map.replace("one", 100);
        System.out.println("replaced = " + replaced);
        System.out.println(map);

        // 遍历元素
        map.forEach((key, value) -> {
            System.out.println(key + " = " + value);
        });
    }
}
