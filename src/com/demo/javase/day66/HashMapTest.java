package com.demo.javase.day66;

import java.util.HashMap;

/**
 * @Author bug菌
 * @Date 2023-11-06 11:57
 */
public class HashMapTest {

    public static void main(String[] args) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("key1", "value1");
        hashMap.put("key2", "value2");
        hashMap.put("key3", "value3");
        System.out.println(hashMap.get("key2")); // 输出：value2
        System.out.println(hashMap.remove("key3")); // 输出：value3
        System.out.println(hashMap.containsKey("key1")); // 输出：true
        System.out.println(hashMap.containsValue("value1")); // 输出：true
        System.out.println(hashMap); // 输出：{key1=value1, key2=value2}
    }
}
