package com.demo.javase.day67;

import java.util.TreeMap;

/**
 * @Author bug菌
 * @Date 2023-11-06 12:12
 */
public class TreeMapPerformanceTest {
    private static final int SIZE = 1000000;

    public static void main(String[] args) {
        System.out.println("TreeMap Performance Test:");
        System.out.println("-------------------------");

        TreeMap<Integer, Integer> map = new TreeMap<>();

        // Insert test
        long start = System.currentTimeMillis();

        for (int i = 0; i < SIZE; i++) {
            map.put(i, i);
        }

        long end = System.currentTimeMillis();

        System.out.println("Insert " + SIZE + " elements time: " + (end - start) + "ms");

        // Search test
        start = System.currentTimeMillis();

        for (int i = 0; i < SIZE; i++) {
            map.get(i);
        }

        end = System.currentTimeMillis();

        System.out.println("Search " + SIZE + " elements time: " + (end - start) + "ms");

        // Delete test
        start = System.currentTimeMillis();

        for (int i = 0; i < SIZE; i++) {
            map.remove(i);
        }

        end = System.currentTimeMillis();

        System.out.println("Delete " + SIZE + " elements time: " + (end - start) + "ms");
    }
}
