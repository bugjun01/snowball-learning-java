package com.demo.javase.day67;

import java.util.Map;
import java.util.TreeMap;

/**
 * @Author bug菌
 * @Date 2023-11-06 12:10
 */
public class TreeMapTest {

    public static void main(String[] args) {
        TreeMap<String, Integer> map = new TreeMap<>();
        map.put("Tom", 85);
        map.put("Jack", 92);
        map.put("Lily", 76);
        map.put("Bob", 88);

        System.out.println("Initial TreeMap:");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }

        map.remove("Lily");

        System.out.println("\nAfter removing Lily:");
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}
