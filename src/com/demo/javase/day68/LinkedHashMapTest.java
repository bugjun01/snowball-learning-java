package com.demo.javase.day68;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @Author bug菌
 * @Date 2023-11-06 12:30
 */
public class LinkedHashMapTest {

    public static void main(String[] args) {
        LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put(1, "apple");
        linkedHashMap.put(4, "banana");
        linkedHashMap.put(2, "orange");
        linkedHashMap.put(3, "pear");

        for (Map.Entry<Integer, String> entry : linkedHashMap.entrySet()) {
            System.out.println("key: " + entry.getKey() + ", value: " + entry.getValue());
        }
    }
}
