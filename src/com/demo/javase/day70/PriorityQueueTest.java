package com.demo.javase.day70;

import java.util.PriorityQueue;

/**
 * @Author bug菌
 * @Date 2023-11-06 16:08
 */
public class PriorityQueueTest {

    public static void main(String[] args) {
        PriorityQueue<Integer> pq = new PriorityQueue<>();
        pq.add(5);
        pq.add(1);
        pq.add(10);
        pq.add(3);
        pq.add(2);

        System.out.println("队列中的元素(从小到大)：");
        while (!pq.isEmpty()) {
            System.out.print(pq.poll() + " ");
        }
    }
}
