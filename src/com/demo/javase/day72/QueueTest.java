package com.demo.javase.day72;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @Author bug菌
 * @Date 2023-11-06 16:25
 */
public class QueueTest {

    public static void main(String[] args) {
        Queue<String> queue = new LinkedList<>();

        queue.add("apple");
        queue.add("banana");
        queue.add("orange");
        queue.add("grape");

        System.out.println("Queue : " + queue);

        System.out.println("Head : " + queue.remove());

        System.out.println("Queue after removal of head : " + queue);

        queue.add("pear");

        System.out.println("Queue after addition of an element : " + queue);

        System.out.println("Head : " + queue.peek());

        System.out.println("Size of queue : " + queue.size());
    }

}
