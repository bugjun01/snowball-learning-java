package com.demo.javase.day74;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Author bug菌
 * @Date 2023-12-27 16:48
 */
public class MyInputStream extends InputStream {
    private byte[] data = {1, 2, 3, 4, 5};
    private int pos = 0;

    @Override
    public int read() throws IOException {
        if (pos < data.length) {
            return data[pos++];
        } else {
            return -1;
        }
    }
}

