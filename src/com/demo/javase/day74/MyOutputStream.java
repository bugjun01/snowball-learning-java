package com.demo.javase.day74;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MyOutputStream extends OutputStream {
    private ByteArrayOutputStream buffer = new ByteArrayOutputStream();

    @Override
    public void write(int b) throws IOException {
        buffer.write(b);
    }

    public byte[] getData() {
        return buffer.toByteArray();
    }
}
