package com.demo.javase.day74;

import java.io.IOException;

/**
 * @Author bug菌
 * @Date 2023-12-27 16:49
 */
public class Test {

    public static void main(String[] args) {
        try {
            MyInputStream in = new MyInputStream();
            MyOutputStream out = new MyOutputStream();

            int b;
            while ((b = in.read()) != -1) {
                out.write(b);
            }

            in.close();
            out.close();

            byte[] data = out.getData();
            for (byte d : data) {
                System.out.print(d + " ");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
