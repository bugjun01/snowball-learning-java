package com.demo.javase.day75;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @Author bug菌
 * @Date 2023-12-27 17:03
 */
public class FileReadWriteTest {

    public static void main(String[] args) {
        // 读取文件内容
        try {
            FileReader reader = new FileReader("./input.txt");
            char[] buffer = new char[1024];
            int length;
            while ((length = reader.read(buffer)) != -1) {
                String content = new String(buffer, 0, length);
                System.out.println(content);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // 写入文件内容
        try {
            FileWriter writer = new FileWriter("./output.txt");
            String content = "Hello, world!";
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
