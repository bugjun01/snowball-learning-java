package com.demo.javase.day76;

import java.io.Serializable;

/**
 * @Author bug菌
 * @Date 2023-12-27 17:11
 */
public class Person implements Serializable {
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}
