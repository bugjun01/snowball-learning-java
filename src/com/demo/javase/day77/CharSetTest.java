package com.demo.javase.day77;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

/**
 * @Author bug菌
 * @Date 2023-12-27 17:24
 */
public class CharSetTest {

    public static void main(String[] args) {
        String str = "Hello, 世界!";
        byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
        System.out.println("UTF-8编码：" + Arrays.toString(bytes));

        String decodeStr = new String(bytes, StandardCharsets.UTF_8);
        System.out.println("UTF-8解码：" + decodeStr);
    }
}