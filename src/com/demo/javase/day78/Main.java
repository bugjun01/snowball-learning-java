package com.demo.javase.day78;

import java.util.Scanner;

/**
 * @Author bug菌
 * @Date 2023-12-27 17:32
 */
public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入第一个数字：");
        int num1 = scanner.nextInt();
        System.out.print("请输入第二个数字：");
        int num2 = scanner.nextInt();
        int sum = num1 + num2;
        System.out.println("两个数字的和为：" + sum);
    }
}
