package com.demo.javase.day79;

import java.io.File;
import java.io.IOException;

/**
 * @Author bug菌
 * @Date 2023-12-27 18:04
 */
public class FileCompressionTest {

    public static void main(String[] args) {
        File sourceFile = new File("./person.txt");
        File compressedFile = new File("./compressed.zip");
        File destinationFile = new File("./destination.txt");
        try {
            FileCompression.compressFile(sourceFile, compressedFile);
            FileCompression.decompressFile(compressedFile, destinationFile);
            System.out.println("文件压缩和解压缩成功");
        } catch (IOException e) {
            System.out.println("文件压缩和解压缩失败");
            e.printStackTrace();
        }
    }
}
