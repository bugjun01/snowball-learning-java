package com.demo.javase.day80;

/**
 * @Author bug菌
 * @Date 2023-12-27 18:30
 */
public class MyRunnable implements Runnable {
    public void run() {
        // 线程执行的代码逻辑
        System.out.println("执行MyRunnable：打印");
    }
}
