package com.demo.javase.day80;

/**
 * @Author bug菌
 * @Date 2023-12-27 18:29
 */
public class ThreadDemo {
    public static void main(String[] args) {
        // 创建线程对象
        MyThread myThread = new MyThread();
        // 启动线程
        myThread.start();

        // 创建线程对象
        MyRunnable myRunnable = new MyRunnable();
        // 创建线程
        Thread thread = new Thread(myRunnable);
        // 启动线程
        thread.start();
    }
}
